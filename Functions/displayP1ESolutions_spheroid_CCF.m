function [] = displayP1ESolutions_spheroid_CCF(Cc,s_Cc,Ellipse,B_,Cam,widthImage,heightImage,ellipsoidColor,ellipseColor,DISPLAY_SPHEROIDS,DISPLAY_CONTACT,DISPLAY_RADII_ENDPOINTS)
% Cc: camera positions solutions
% s_Cc: sigma values (replicated scalar value)
% Ellipse: ellipse structure
% B_: back-projection cone B'
% Cam: camera structure
    
    L1 = widthImage; L2 = heightImage;
    I = [-L1/2 -L1/2 L1/2 L1/2 ; -L2/2 L2/2 L2/2 -L2/2 ; Cam.f Cam.f Cam.f Cam.f];
    for i=1:length(s_Cc)
        Delta = -Cc(:,i);
        C = Cc(:,i)';
        E = Cam.Ec;

        Ac = (s_Cc(i)/(1-s_Cc(i)*Delta'*B_*Delta))*(B_-s_Cc(i)*B_*Delta*Delta'*B_);
        [Pc_ell Aell] = eig(Ac);
        [~,order] = sort(diag(Aell));
        Pc_ell = Pc_ell(:,order);
        if det(Pc_ell)<0
            Pc_ell = -Pc_ell;
        end
        if Pc_ell(1,1)<0
            Pc_ell(:,1:2) = -Pc_ell(:,1:2);
        end
        if Pc_ell(3,2)<0
            Pc_ell(:,2:3) = -Pc_ell(:,2:3);
        end
        Aell = Aell(order,order);
        if DISPLAY_SPHEROIDS && ((i==1) || (i==length(s_Cc)))
            Qell = diag([1/Aell(1,1) 1/Aell(2,2) 1/Aell(3,3)]);
            Qc = Pc_ell*Qell*Pc_ell';
            Qc = [Qc [0;0;0] ; [0 0 0 1]];
            T = [1 0 0 C(1); 0 1 0 C(2); 0 0 1 C(3); 0 0 0 1];
            Qc = T*Qc*T';
        end

        C_(i,:) = C;
        M1(i,:) = C+[sqrt(1/Aell(1,1)) 0 0]*Pc_ell';
        M2(i,:) = C-[sqrt(1/Aell(1,1)) 0 0]*Pc_ell';
        M3(i,:) = C+[0 sqrt(1/Aell(2,2)) 0]*Pc_ell';
        M4(i,:) = C-[0 sqrt(1/Aell(2,2)) 0]*Pc_ell';
        M5(i,:) = C+[0 0 sqrt(1/Aell(3,3))]*Pc_ell';
        M6(i,:) = C-[0 0 sqrt(1/Aell(3,3))]*Pc_ell';
        D_ = [Ellipse.xc ; Ellipse.yc ; Ellipse.zc]-repmat(E,1,length(Ellipse.xc));
        rts = [];
        for j=1:length(D_)
            poly = [D_(:,j)'*Ac*D_(:,j) 2*D_(:,j)'*Ac*Delta Delta'*Ac*Delta-1];
            rts = [rts mean(real(roots(poly)))];
        end
        P = rts.*D_;
        
        if i==1
            % Display camera center
            pcshow(E',[0 0 0],'markerSize',60)
        end
        
        if DISPLAY_SPHEROIDS && ((i==1) || (i==length(s_Cc)))
            % Display spheroid
            hold on
            displayEllipsoid(Qc,ellipsoidColor);
            if DISPLAY_CONTACT
                hold on
                line(P(1,:),P(2,:),P(3,:),'Color',[0.8500 0.3250 0.0980],'LineWidth',2)
            end
        end
    end

    % Display
    % Points
    hold on
    pcshow(C_([1 end],:),repmat(0,2,3),'markerSize',60)
    % Ellipse
    hold on
    line(D_(1,:),D_(2,:),D_(3,:),'Color',ellipseColor,'LineWidth',2)
    if DISPLAY_RADII_ENDPOINTS
        % Points
        hold on
        pcshow([M1;M2;M3;M4;M5;M6],[repmat([0 0.5 1],i,1) ; repmat([0 0.5 1],i,1) ; repmat([1 0 0],i,1) ; repmat([1 0 0],i,1) ; repmat([0 1 0],i,1) ; repmat([0 1 0],i,1)],'markerSize',60)
    end
    % Image plane
    hold on
    patch('Faces',[1 2 3 4],'Vertices',I','FaceColor',[0.5 0.5 0.5],'FaceAlpha',0.3);
    % Params
    text(E(1),E(2),E(3),'E','FontSize',20)
    text(C_(1,1),C_(1,2),C_(1,3),'C_1','FontSize',20)
    text(C_(end,1),C_(end,2),C_(end,3),'C_2','FontSize',20)
    set(gcf,'color','white')
    set(gca,'color','white')
    set(gca,'XColor','black')
    set(gca,'YColor','black')
    set(gca,'ZColor','black')
end

