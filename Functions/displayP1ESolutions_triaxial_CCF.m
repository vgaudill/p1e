function [] = displayP1ESolutions_triaxial_CCF(Cc,s_Cc,Ellipse,B_,Cam,mode,sizeAxes,widthImage,heightImage,ellipsoidColor,ellipseColor,coneColor,DISPLAY_CONTACT,OPTIMIZE_MEMORY)
% Cc: camera positions solutions
% s_Cc: sigma values (discretized interval)
% Ellipse: ellipse structure
% B_: back-projection cone B'
% Cam: camera structure
% mode: 'dynamic' or 'static' display
% OPTIMIZE_MEMORY: 'true' (display points) or 'false' (display lines)
    
    L1 = widthImage; L2 = heightImage;
    I = [-L1/2 -L1/2 L1/2 L1/2 ; -L2/2 L2/2 L2/2 -L2/2 ; Cam.f Cam.f Cam.f Cam.f];
    for i=1:length(s_Cc)
        if(i>1)
            clf
        end

        Delta = -Cc(:,i);
        C = Cc(:,i)';
        E = Cam.Ec;

        A = (s_Cc(i)/(1-s_Cc(i)*Delta'*B_*Delta))*(B_-s_Cc(i)*B_*Delta*Delta'*B_);
        [Pc_ell Aell] = eig(A);
        [~,order] = sort(diag(Aell));
        Pc_ell = Pc_ell(:,order);
        if det(Pc_ell)<0
            Pc_ell = -Pc_ell;
        end
        if Pc_ell(1,1)<0
            Pc_ell(:,1:2) = -Pc_ell(:,1:2);
        end
        if Pc_ell(3,2)<0
            Pc_ell(:,2:3) = -Pc_ell(:,2:3);
        end
        Aell = Aell(order,order);
        Qell = diag([1/Aell(1,1) 1/Aell(2,2) 1/Aell(3,3)]);
        Qc = Pc_ell*Qell*Pc_ell';
        Qc = [Qc [0;0;0] ; [0 0 0 1]];
        T = [1 0 0 C(1); 0 1 0 C(2); 0 0 1 C(3); 0 0 0 1];
        Qc = T*Qc*T';

        C_(i,:) = C;
        M1(i,:) = C+[sqrt(1/Aell(1,1)) 0 0]*Pc_ell';
        M2(i,:) = C-[sqrt(1/Aell(1,1)) 0 0]*Pc_ell';
        M3(i,:) = C+[0 sqrt(1/Aell(2,2)) 0]*Pc_ell';
        M4(i,:) = C-[0 sqrt(1/Aell(2,2)) 0]*Pc_ell';
        M5(i,:) = C+[0 0 sqrt(1/Aell(3,3))]*Pc_ell';
        M6(i,:) = C-[0 0 sqrt(1/Aell(3,3))]*Pc_ell';
        D_ = [Ellipse.xc ; Ellipse.yc ; Ellipse.zc]-repmat(E,1,length(Ellipse.xc));
        rts = [];
        for j=1:length(D_)
            poly = [D_(:,j)'*A*D_(:,j) 2*D_(:,j)'*A*Delta Delta'*A*Delta-1];
            rts = [rts mean(real(roots(poly)))];
        end
        P = rts.*D_;
            
        if strcmp(mode,'dynamic')
            if OPTIMIZE_MEMORY
                % Points
                pcshow([E';C_;M1;M2;M3;M4;M5;M6],[0 0 0 ; repmat([0 0 0],i,1) ; repmat([0 0.5 1],i,1) ; repmat([0 0.5 1],i,1) ; repmat([1 0 0],i,1) ; repmat([1 0 0],i,1) ; repmat([0 1 0],i,1) ; repmat([0 1 0],i,1)],'markerSize',60)
                % Ellipse
                hold on
                line(D_(1,:),D_(2,:),D_(3,:),'Color',ellipseColor,'LineWidth',2)
            else
                % Camera center
                pcshow(E',[0 0 0],'markerSize',30);
                % Ellipse
                hold on
                line(D_(1,:),D_(2,:),D_(3,:),'Color',ellipseColor,'LineWidth',2)
                % Ellipsoid center
                hold on
                line(C_(:,1),C_(:,2),C_(:,3),'Color',[0 0 0],'LineWidth',3)
                % Radius 1, endpoint 1
                hold on
                line(M1(:,1),M1(:,2),M1(:,3),'Color',[0 0.5 1],'LineWidth',3)
                % Radius 1, endpoint 2
                hold on
                line(M2(:,1),M2(:,2),M2(:,3),'Color',[0 0.5 1],'LineWidth',3)
                % Radius 2, endpoint 1
                hold on
                line(M3(:,1),M3(:,2),M3(:,3),'Color',[1 0 0],'LineWidth',3)
                % Radius 2, endpoint 2
                hold on
                line(M4(:,1),M4(:,2),M4(:,3),'Color',[1 0 0],'LineWidth',3)
                % Radius 3, endpoint 1
                hold on
                line(M5(:,1),M5(:,2),M5(:,3),'Color',[0 1 0],'LineWidth',3)
                % Radius 3, endpoint 2
                hold on
                line(M6(:,1),M6(:,2),M6(:,3),'Color',[0 1 0],'LineWidth',3)
            end
            % Image plane
            hold on
            patch('Faces',[1 2 3 4],'Vertices',I','FaceColor',[0.5 0.5 0.5],'FaceAlpha',0.3);
            % Ellipsoid
            hold on
            displayEllipsoid(Qc,ellipsoidColor);
            if DISPLAY_CONTACT
                hold on
                line(P(1,:),P(2,:),P(3,:),'Color',[0.8500 0.3250 0.0980],'LineWidth',2)
            end
            % Cone
            hold on
            f = [1 2 3];
            for j=1:length(P)-1
                v = [P(:,j:j+1)' ; [0 0 0]];
                patch('Faces',f,'Vertices',v,'FaceColor',coneColor,'EdgeColor','None','FaceAlpha',0.3)
                hold on
            end
            v = [P(:,end)' ; P(:,1)' ; [0 0 0]];
            patch('Faces',f,'Vertices',v,'FaceColor',coneColor,'EdgeColor','None','FaceAlpha',0.3)
            % Params
            text(E(1),E(2),E(3),'E','FontSize',20)
            text(C(1),C(2),C(3)+0.2,'C','FontSize',20)
            set(gcf,'color','white')
            set(gca,'color','white')
            set(gca,'XColor','black')
            set(gca,'YColor','black')
            set(gca,'ZColor','black')
            drawnow()
        end
    end

    % Display static results
    if strcmp(mode,'static')
        if OPTIMIZE_MEMORY
            % Points
            pcshow([E';C_;M1;M2;M3;M4;M5;M6],[0 0 0 ; repmat([0 0 0],i,1) ; repmat([0 0.5 1],i,1) ; repmat([0 0.5 1],i,1) ; repmat([1 0 0],i,1) ; repmat([1 0 0],i,1) ; repmat([0 1 0],i,1) ; repmat([0 1 0],i,1)],'markerSize',60)
            % Ellipse
            hold on
            line(D_(1,:),D_(2,:),D_(3,:),'Color',ellipseColor,'LineWidth',2)
        else
            % Camera center
            pcshow(E',[0 0 0],'markerSize',30);
            % Ellipse
            hold on
            line(D_(1,:),D_(2,:),D_(3,:),'Color',ellipseColor,'LineWidth',2)
            % Ellipsoid center
            hold on
            line(C_(:,1),C_(:,2),C_(:,3),'Color',[0 0 0],'LineWidth',3)
            % Radius 1, endpoint 1
            hold on
            line(M1(:,1),M1(:,2),M1(:,3),'Color',[0 0.5 1],'LineWidth',3)
            % Radius 1, endpoint 2
            hold on
            line(M2(:,1),M2(:,2),M2(:,3),'Color',[0 0.5 1],'LineWidth',3)
            % Radius 2, endpoint 1
            hold on
            line(M3(:,1),M3(:,2),M3(:,3),'Color',[1 0 0],'LineWidth',3)
            % Radius 2, endpoint 2
            hold on
            line(M4(:,1),M4(:,2),M4(:,3),'Color',[1 0 0],'LineWidth',3)
            % Radius 3, endpoint 1
            hold on
            line(M5(:,1),M5(:,2),M5(:,3),'Color',[0 1 0],'LineWidth',3)
            % Radius 3, endpoint 2
            hold on
            line(M6(:,1),M6(:,2),M6(:,3),'Color',[0 1 0],'LineWidth',3)
        end
        % Image plane
        hold on
        patch('Faces',[1 2 3 4],'Vertices',I','FaceColor',[0.5 0.5 0.5],'FaceAlpha',0.3);
        % Params
        text(E(1),E(2),E(3),'E','FontSize',20)
        set(gcf,'color','white')
        set(gca,'color','white')
        set(gca,'XColor','black')
        set(gca,'YColor','black')
        set(gca,'ZColor','black')
    end
end