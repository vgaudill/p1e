function [s] = getSigma_spheroid(A,B_)
% A: ellipsoid orientation & shape
% B_: cone orientation & shape
% s: sigma (scalar unknown in the Cone Alignment Equation)
    lA = sort(eig(A));
    lB = sort(eig(B_));
    
    if abs(lA(3)-lA(2))<abs(lA(3)-lA(1)) % single: 1 | double: 2,3
        lAs = lA(1);
        lAd = (lA(2)+lA(3))/2;
    else % single: 3 | double: 1,2
        lAs = lA(3);
        lAd = (lA(1)+lA(2))/2;
    end
    
    if lB(1)*lB(2)>0 % same sign: 1,2 | opposite sign: 3
        lB3 = lB(3);
        if abs(lB(1))>abs(lB(2))
            lB1 = lB(1);
            lB2 = lB(2);
        else % abs(lB(2))>abs(lB(1))
            lB1 = lB(2);
            lB2 = lB(1);
        end
    else % same sign: 2,3 | opposite sign: 1
        lB3 = lB(1);
        if abs(lB(2))>abs(lB(3))
            lB1 = lB(2);
            lB2 = lB(3);
        else % abs(lB(3))>abs(lB(2))
            lB1 = lB(3);
            lB2 = lB(2);
        end
    end
    
    if lAs<lAd
        s = (lAs*lB1)/(lB2*lB3);
    else % lAs>lAd
        s = (lAs*lB2)/(lB1*lB3);
    end
end
