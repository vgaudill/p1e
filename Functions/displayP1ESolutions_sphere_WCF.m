function [] = displayP1ESolutions_sphere_WCF(Ellipsoid,B_,ellipsoidColor)
    Pw_ell = Ellipsoid.Rw_ell; Aell = Ellipsoid.Aell;
    [~,order] = sort(diag(Aell));
    Pw_ell = Pw_ell(:,order);
    if det(Pw_ell)<0
        Pw_ell = -Pw_ell;
    end
    if Pw_ell(1,1)<0
        Pw_ell(:,1:2) = -Pw_ell(:,1:2);
    end
    if Pw_ell(3,2)<0
        Pw_ell(:,2:3) = -Pw_ell(:,2:3);
    end
    Aell = Aell(order,order);
    Qell = diag([1/Aell(1,1) 1/Aell(2,2) 1/Aell(3,3)]);
    Qw = Pw_ell*Qell*Pw_ell';
    Qw = [Qw [0;0;0] ; [0 0 0 1]];
    T = [1 0 0 Ellipsoid.Cw(1); 0 1 0 Ellipsoid.Cw(2); 0 0 1 Ellipsoid.Cw(3); 0 0 0 1];
    Qw = T*Qw*T';
    
    lambda_A_triple = mean(diag(Aell));
    B_cone = sort(eig(B_));
    if abs(B_cone(3)-B_cone(2))<abs(B_cone(3)-B_cone(1)) % single: 1 | double: 2,3
        lambda_B_single = B_cone(1);
        lambda_B_double = mean(B_cone([2 3]));
    else % single: 3 | double: 1,2
        lambda_B_single = B_cone(3);
        lambda_B_double = mean(B_cone([1 2]));
    end
    normD = sqrt(1/lambda_A_triple*(1-lambda_B_double/lambda_B_single));
    
    % Display
    pcshow(Ellipsoid.Cw',[0 0 0],'markerSize',60)
    hold on
    q = randrot(1000,1);
    pt = rotatepoint(q, [normD 0 0])+Ellipsoid.Cw';
    pcshow(pt,[0 0 0],'markerSize',60)
    hold on
    displaySpheroid(Qw,ellipsoidColor);
    % Params
    xlim([-8.51 4.51])
    ylim([-3.16 11.16])
    zlim([-6.31 8.4])
    text(Ellipsoid.Cw(1),Ellipsoid.Cw(2),Ellipsoid.Cw(3),'C','FontSize',20)
    text(-8.25,11,-3,sprintf('a=%.02f',1/sqrt(Aell(1,1))),'FontSize',10,'Color',[0 0.5 1])
    text(-8.25,11,-4,sprintf('b=%.02f',1/sqrt(Aell(2,2))),'FontSize',10,'Color',[1 0 0])
    text(-8.25,11,-5,sprintf('c=%.02f',1/sqrt(Aell(3,3))),'FontSize',10,'Color',[0 1 0])
    text(-4,11,9,'\bf Sphere: sphere of solutions','FontSize',15,'Color',[0 0 0])
    set(gcf,'color','white')
    set(gca,'color','white')
    set(gca,'XColor','black')
    set(gca,'YColor','black')
    set(gca,'ZColor','black')
end