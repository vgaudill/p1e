function [Camera] = createCamera(Ew,Rw_c,f)
% Generate camera.
% Ew: position in World Coordinate Frame (WCF) ([3x1])
% Rw_c: orientation in WCF ([3x3])
% f: camera plane equation in Camera Coordinate Frame (CCF) is z=f

    % Location
    Camera.Ew = Ew;
    Camera.Ec = [0;0;0];

    % Orientation
    Camera.Rw_c = Rw_c;
    
    % Distance center to image plane (virtual)
    Camera.f = f;
    
    % Origin of WCF in CCF
    Camera.Oc = Rw_c'*([0;0;0]-Ew); % Ow = [0;0;0]  
    
    % Intrinsics/Extrinsics
    K = [f 0 0 ; 0 f 0 ; 0 0 1];
    Camera.K = K;
    R = Camera.Rw_c';
    T = -R*Camera.Ew;
    P = K*[R T];
    Camera.R = R;
    Camera.T = T;
    Camera.P = P/P(end,end);
end