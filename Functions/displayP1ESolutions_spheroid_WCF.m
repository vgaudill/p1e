function [] = displayP1ESolutions_spheroid_WCF(Cc,s_Cc,Ellipsoid,B_,ellipsoidColor)
% Cc: camera positions solutions
% s_Cc: sigma values (replicated scalar value)
% Ellipse: ellipse structure
% B_: back-projection cone B'
% Cam: camera structure
    
    [Pw_ell Aell] = eig(Ellipsoid.Aw);
    [~,order] = sort(diag(Aell));
    Pw_ell = Pw_ell(:,order);
    if det(Pw_ell)<0
        Pw_ell = -Pw_ell;
    end
    if Pw_ell(1,1)<0
        Pw_ell(:,1:2) = -Pw_ell(:,1:2);
    end
    if Pw_ell(3,2)<0
        Pw_ell(:,2:3) = -Pw_ell(:,2:3);
    end
    Aell = Aell(order,order);
    Qell = diag([1/Aell(1,1) 1/Aell(2,2) 1/Aell(3,3)]);
    Qw = Pw_ell*Qell*Pw_ell';
    Qw = [Qw [0;0;0] ; [0 0 0 1]];
    T = [1 0 0 Ellipsoid.Cw(1); 0 1 0 Ellipsoid.Cw(2); 0 0 1 Ellipsoid.Cw(3); 0 0 0 1];
    Qw = T*Qw*T';

    Delta_w = NaN(3,8*length(s_Cc));
    for i=1:length(s_Cc)
        Delta_c = -Cc(:,i);

        Ac = (s_Cc(i)/(1-s_Cc(i)*Delta_c'*B_*Delta_c))*(B_-s_Cc(i)*B_*Delta_c*Delta_c'*B_);
        Ac = (Ac+Ac')/2;
        [Pc_ell Aell] = eig(Ac);
        [~,order] = sort(diag(Aell));
        Pc_ell = Pc_ell(:,order);
        if det(Pc_ell)<0
            Pc_ell = -Pc_ell;
        end
%         if Pc_ell(1,1)<0
%             Pc_ell(:,1:2) = -Pc_ell(:,1:2);
%         end
%         if Pc_ell(3,2)<0
%             Pc_ell(:,2:3) = -Pc_ell(:,2:3);
%         end
        Aell = Aell(order,order);
        
        if abs(Aell(3,3)-Aell(2,2))<abs(Aell(3,3)-Aell(1,1)) % single: 1 | double: 2,3
            sgl = 1;
            V1 = Pc_ell(:,2); V2 = Pc_ell(:,3);
            Pc_ell(:,2) = cosd((i-1)/length(s_Cc)*360)*V1+sind((i-1)/length(s_Cc)*360)*V2;
            Pc_ell(:,3) = -sind((i-1)/length(s_Cc)*360)*V1+cosd((i-1)/length(s_Cc)*360)*V2;
        else % single: 3 | double: 1,2
            sgl = 3;
            V1 = Pc_ell(:,1); V2 = Pc_ell(:,2);
            Pc_ell(:,1) = cosd((i-1)/length(s_Cc)*360)*V1+sind((i-1)/length(s_Cc)*360)*V2;
            Pc_ell(:,2) = -sind((i-1)/length(s_Cc)*360)*V1+cosd((i-1)/length(s_Cc)*360)*V2;
        end

        Pc_ell1 = Pc_ell;
        R = Pw_ell*Pc_ell1';
        Dw = R*Delta_c;
        Ew_(:,(i-1)*8+1) = Dw+Ellipsoid.Cw;

        Pc_ell2 = Pc_ell;
        Pc_ell2(:,1)=-Pc_ell2(:,1);
        Pc_ell2(:,2)=-Pc_ell2(:,2);
        R = Pw_ell*Pc_ell2';
        Dw = R*Delta_c;
        Ew_(:,(i-1)*8+2) = Dw+Ellipsoid.Cw;

        Pc_ell3 = Pc_ell;
        Pc_ell3(:,1)=-Pc_ell3(:,1);
        Pc_ell3(:,3)=-Pc_ell3(:,3);
        R = Pw_ell*Pc_ell3';
        Dw = R*Delta_c;
        Ew_(:,(i-1)*8+3) = Dw+Ellipsoid.Cw;

        Pc_ell4 = Pc_ell;
        Pc_ell4(:,2)=-Pc_ell4(:,2);
        Pc_ell4(:,3)=-Pc_ell4(:,3);
        R = Pw_ell*Pc_ell4';
        Dw = R*Delta_c;
        Ew_(:,(i-1)*8+4) = Dw+Ellipsoid.Cw;
        
        if sgl==1
            Pc_ell = Pc_ell(:,[1 3 2]); % swap eigenvectors corresponding to identical eigenvalues
        else % sgl==3
            Pc_ell = Pc_ell(:,[2 1 3]); % swap eigenvectors corresponding to identical eigenvalues
        end
        
        Pc_ell5 = Pc_ell;
        R = Pw_ell*Pc_ell5';
        Dw = R*Delta_c;
        Ew_(:,(i-1)*8+5) = Dw+Ellipsoid.Cw;

        Pc_ell6 = Pc_ell;
        Pc_ell6(:,1)=-Pc_ell6(:,1);
        Pc_ell6(:,2)=-Pc_ell6(:,2);
        R = Pw_ell*Pc_ell6';
        Dw = R*Delta_c;
        Ew_(:,(i-1)*8+6) = Dw+Ellipsoid.Cw;

        Pc_ell7 = Pc_ell;
        Pc_ell7(:,1)=-Pc_ell7(:,1);
        Pc_ell7(:,3)=-Pc_ell7(:,3);
        R = Pw_ell*Pc_ell7';
        Dw = R*Delta_c;
        Ew_(:,(i-1)*8+7) = Dw+Ellipsoid.Cw;

        Pc_ell8 = Pc_ell;
        Pc_ell8(:,2)=-Pc_ell8(:,2);
        Pc_ell8(:,3)=-Pc_ell8(:,3);
        R = Pw_ell*Pc_ell8';
        Dw = R*Delta_c;
        Ew_(:,i*8) = Dw+Ellipsoid.Cw;
    end

    % Display
    % Points
    pcshow([Ew_' ; Ellipsoid.Cw'],repmat(0,length(Ew_)+1,3),'markerSize',60)
    % Ellipsoid
    hold on
    displayEllipsoid(Qw,ellipsoidColor);
    % Params
%     xlim([-6 4])
%     ylim([-1 4])
%     zlim([0 10])
    text(Ellipsoid.Cw(1),Ellipsoid.Cw(2),Ellipsoid.Cw(3),'C','FontSize',20)
    set(gcf,'color','white')
    set(gca,'color','white')
    set(gca,'XColor','black')
    set(gca,'YColor','black')
    set(gca,'ZColor','black')
end