function [Info] = getEllipsoidInCCF(Ellipsoid,Camera)
% Compute information related to the link between Ellipsoid Coordinate Frame 
% (ECF) and Camera Coordinate Frame (CCF).

    Info.Rc_ell = Camera.Rw_c'*Ellipsoid.Rw_ell;
    Info.EAc_ell = SpinCalc('DCMtoEA313',Info.Rc_ell,1,0);
    Info.Ac = Info.Rc_ell*Ellipsoid.Aell*Info.Rc_ell';

    Info.Cc = Camera.Rw_c'*(Ellipsoid.Cw-Camera.Ew);
    Info.Eell = Ellipsoid.Rw_ell'*(Camera.Ew-Ellipsoid.Cw);

    Qell = [Ellipsoid.a^2 0 0 ; 0 Ellipsoid.b^2 0 ; 0 0 Ellipsoid.c^2];
    Qc = Info.Rc_ell*Qell*Info.Rc_ell';
    Qc = [Qc [0;0;0] ; [0 0 0 1]];
    T = [1 0 0 Info.Cc(1); 0 1 0 Info.Cc(2); 0 0 1 Info.Cc(3); 0 0 0 1];
    Info.Qc = T*Qc*T';
    
end

