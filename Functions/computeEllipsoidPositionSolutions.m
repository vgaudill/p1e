function [Cc,s_Cc] = computeEllipsoidPositionSolutions(Ellipsoid,Cam,B_,s)
% Compute ellipsoid position solutions from A (Ellipsoid), B' (B_) and sigma (s).

    Info = getEllipsoidInCCF(Ellipsoid,Cam);
    Delta_gt = -Info.Cc;
        
    trA_1 = trace(inv(Ellipsoid.Aw));
    trA = trace(Ellipsoid.Aw);
    detA = det(Ellipsoid.Aw);
    D2 = nan(3,length(s));
    D = nan(3,length(s)*4);
    for i=1:length(s)
        B = s(i)*B_;

        mu = -sqrt(det(B)/detA);
        V = [trA_1-mu*trace(inv(B)) ; ...
             1-mu ; ...
             trace(B)-mu*trA];
        [Pc_cone lambdaB] = eig(B,'vector');
        M = [1 1 1 ; lambdaB' ; lambdaB'.^2];

        D2(:,i) = abs(inv(M)*V);
%             D2(:,1000) = abs(D2(:,1000));
%             if abs(D2(1,i))<10^(-10)
%                 D2(1,i) = max([D2(1,i) 0]);
%             end

%         Info = getEllipsoidInCCF(Ellipsoid,Cam);
        D(:,i)                 = Pc_cone*([ 1; 1; sign(Delta_gt(3))].*sqrt(D2(:,i)));
        D(:,length(s)*2-(i-1)) = Pc_cone*([-1; 1; sign(Delta_gt(3))].*sqrt(D2(:,i)));
        D(:,length(s)*2+i)     = Pc_cone*([-1;-1; sign(Delta_gt(3))].*sqrt(D2(:,i)));
        D(:,length(s)*4-(i-1)) = Pc_cone*([ 1;-1; sign(Delta_gt(3))].*sqrt(D2(:,i)));
    end
    s_Cc = s([1:length(s) length(s):-1:1 1:length(s) length(s):-1:1]);
    Cc = -D;
end

