function [Ew,s_Ew] = computeCameraPositionSolutions(Ellipsoid,Cam,B_,s)
% Compute camera position solutions from A (Ellipsoid), B' (B_) and sigma (s).
    
    trA_1 = trace(inv(Ellipsoid.Aw));
    trA = trace(Ellipsoid.Aw);
    detA = det(Ellipsoid.Aw);
    D2 = nan(3,length(s));
    D = nan(3,length(s)*4);
    for i=1:length(s)
        B = s(i)*B_;

        mu = -sqrt(det(B)/detA);
        V = [trA_1-mu*trace(inv(B)) ; ...
             1-mu ; ...
             trace(B)-mu*trA];
        [Pw_ell lambdaA] = eig(Ellipsoid.Aw,'vector');
        M = [1 1 1 ; lambdaA' ; lambdaA'.^2];

        D2(:,i) = abs(inv(M)*V);
        
        D(:,i)                 = Pw_ell*([ 1; 1; 1].*sqrt(D2(:,i)));
        D(:,length(s)*2-(i-1)) = Pw_ell*([ 1; 1;-1].*sqrt(D2(:,i)));
        D(:,length(s)*2+i)     = Pw_ell*([-1; 1;-1].*sqrt(D2(:,i)));
        D(:,length(s)*4-(i-1)) = Pw_ell*([-1; 1; 1].*sqrt(D2(:,i)));
        D(:,length(s)*4+i)     = Pw_ell*([ 1;-1; 1].*sqrt(D2(:,i)));
        D(:,length(s)*6-(i-1)) = Pw_ell*([ 1;-1;-1].*sqrt(D2(:,i)));
        D(:,length(s)*6+i)     = Pw_ell*([-1;-1;-1].*sqrt(D2(:,i)));
        D(:,length(s)*8-(i-1)) = Pw_ell*([-1;-1; 1].*sqrt(D2(:,i)));
    end
    Ew = repmat(Ellipsoid.Cw,1,length(D)) + D;
    s_Ew = s([1:length(s) length(s):-1:1 1:length(s) length(s):-1:1 1:length(s) length(s):-1:1 1:length(s) length(s):-1:1]);
end