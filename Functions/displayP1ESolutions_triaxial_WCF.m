function [] = displayP1ESolutions_triaxial_WCF(Ew,s_Ew,Ellipsoid,mode,ellipsoidColor,OPTIMIZE_MEMORY)

    for i=1:length(s_Ew)
        if(i>1)
            clf
        end

        Delta = Ew(:,i);
        C = Ellipsoid.Cw;
        E = Ew(:,i)';

        A = Ellipsoid.Aw;

        E_(i,:) = E;

        if strcmp(mode,'dynamic')
            if OPTIMIZE_MEMORY
                pcshow([E_;C'],[0 0 0],'markerSize',60)
            else
                pcshow(C',[0 0 0],'markerSize',60)
                if i<=(length(s_Ew)/2)
                    hold on
                    line(E_(:,1),E_(:,2),E_(:,3),'Color',[0 0 0],'LineWidth',3)
                else
                    hold on
                    line(E_(1:(length(s_Ew)/2),1),E_(1:(length(s_Ew)/2),2),E_(1:(length(s_Ew)/2),3),'Color',[0 0 0],'LineWidth',3)
                    hold on
                    line(E_((length(s_Ew)/2+1):end,1),E_((length(s_Ew)/2+1):end,2),E_((length(s_Ew)/2+1):end,3),'Color',[0 0 0],'LineWidth',3)
                end
            end
            hold on
            displayEllipsoid(Ellipsoid.Qw,ellipsoidColor);
            xlim([-8.4212 4.4213])
            ylim([-3 11])
            zlim([-4.5 6.5])
            text(E(1),E(2),E(3)+0.2,'E','FontSize',20)
            text(C(1),C(2),C(3),'C','FontSize',20)
            set(gcf,'color','white')
            set(gca,'color','white')
            set(gca,'XColor','black')
            set(gca,'YColor','black')
            set(gca,'ZColor','black')
            drawnow
        end
    end
    
    if strcmp(mode,'static')
        if OPTIMIZE_MEMORY
            pcshow([E_;C'],[0 0 0],'markerSize',60)
        else
            pcshow(C',[0 0 0],'markerSize',60)
            if i<=(length(s_Ew)/2)
                hold on
                line(E_(:,1),E_(:,2),E_(:,3),'Color',[0 0 0],'LineWidth',3)
            else
%                 hold on
%                 line(E_(1:(length(s_Ew)/2),1),E_(1:(length(s_Ew)/2),2),E_(1:(length(s_Ew)/2),3),'Color',[0 0 0],'LineWidth',3)
%                 hold on
%                 line(E_((length(s_Ew)/2+1):end,1),E_((length(s_Ew)/2+1):end,2),E_((length(s_Ew)/2+1):end,3),'Color',[0 0 0],'LineWidth',3)
                hold on
                line(E_(1:(length(s_Ew)/4),1),E_(1:(length(s_Ew)/4),2),E_(1:(length(s_Ew)/4),3),'Color',[0 0 0],'LineWidth',3)
                hold on
                line(E_((length(s_Ew)/4+1):(length(s_Ew)/2),1),E_((length(s_Ew)/4+1):(length(s_Ew)/2),2),E_((length(s_Ew)/4+1):(length(s_Ew)/2),3),'Color',[0 0 0],'LineWidth',3)
                hold on
                line(E_((length(s_Ew)/2+1):(3*length(s_Ew)/4),1),E_((length(s_Ew)/2+1):(3*length(s_Ew)/4),2),E_((length(s_Ew)/2+1):(3*length(s_Ew)/4),3),'Color',[0 0 0],'LineWidth',3)
                hold on
                line(E_((3*length(s_Ew)/4+1):end,1),E_((3*length(s_Ew)/4+1):end,2),E_((3*length(s_Ew)/4+1):end,3),'Color',[0 0 0],'LineWidth',3)
                
            end
        end
        hold on
        displayEllipsoid(Ellipsoid.Qw,ellipsoidColor);
%         xlim([-8.4212 4.4213])
%         ylim([-3 11])
%         zlim([-4.5 6.5])
        text(C(1),C(2),C(3),'C','FontSize',20)
        set(gcf,'color','white')
        set(gca,'color','white')
        set(gca,'XColor','black')
        set(gca,'YColor','black')
        set(gca,'ZColor','black')
        drawnow
    end
end