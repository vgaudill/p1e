function [] = displayP1EScene(CF,Ellipsoid,Ellipse,Camera,ellipsoidColor,ellipseColor,coneColor,sizeAxes,widthImage,heightImage,plotCam,camColor,camSize)
% CF: coordinate frame (World: 'WCF', or Camera: 'CCF')
% Camera: 'Camera' structure
% Ellipsoid: 'Ellipsoid' structure

    L1 = widthImage;
    L2 = heightImage;
    
    % Image corners
    Ic = [-L1/2 -L1/2 L1/2 L1/2 ; -L2/2 L2/2 L2/2 -L2/2 ; Camera.f Camera.f Camera.f Camera.f];
    
    % Origin of the world 
    Ow = [0;0;0];

    % Compute coordinates
    switch CF
        case 'WCF'
            I = Camera.Rw_c*(Ic-repmat(Camera.Oc,1,4));
            O = Ow;
            E = Camera.Ew;
            C = Ellipsoid.Cw;
            Q = Ellipsoid.Qw;
            x = Ellipse.xw;
            y = Ellipse.yw;
            z = Ellipse.zw;
            X_WCF = [O O+sizeAxes*[1;0;0]];
            Y_WCF = [O O+sizeAxes*[0;1;0]];
            Z_WCF = [O O+sizeAxes*[0;0;1]];
            X_CCF = [E E+sizeAxes*Camera.Rw_c(:,1)];
            Y_CCF = [E E+sizeAxes*Camera.Rw_c(:,2)];
            Z_CCF = [E E+sizeAxes*Camera.Rw_c(:,3)];
            X_ECF = [C C+sizeAxes*Ellipsoid.Rw_ell(:,1)];
            Y_ECF = [C C+sizeAxes*Ellipsoid.Rw_ell(:,2)];
            Z_ECF = [C C+sizeAxes*Ellipsoid.Rw_ell(:,3)];
            CamOri = Camera.Rw_c';
            CamLoc = Camera.Ew;
            CamLoc = real(Camera.Ew);
        case 'CCF'
            I = Ic;
            O = Camera.Oc;
            E = Camera.Ec;
            Info = getEllipsoidInCCF(Ellipsoid,Camera);
            C = Info.Cc;
            Q = Info.Qc;
            x = Ellipse.xc;
            y = Ellipse.yc;
            z = Ellipse.zc;
            X_WCF = [O O+sizeAxes*Camera.Rw_c(1,:)'];
            Y_WCF = [O O+sizeAxes*Camera.Rw_c(2,:)'];
            Z_WCF = [O O+sizeAxes*Camera.Rw_c(3,:)'];
            X_CCF = [E E+sizeAxes*[1;0;0]];
            Y_CCF = [E E+sizeAxes*[0;1;0]];
            Z_CCF = [E E+sizeAxes*[0;0;1]];
            X_ECF = [C C+sizeAxes*Info.Rc_ell(:,1)];
            Y_ECF = [C C+sizeAxes*Info.Rc_ell(:,2)];
            Z_ECF = [C C+sizeAxes*Info.Rc_ell(:,3)];
            CamOri = eye(3);
            CamLoc = [0;0;0];
        otherwise
            error('Coordinates frame is not recognized.');
    end    
    
    % Origins
    switch CF
        case 'CCF'
            Pts = E';
            color = zeros(1,3);
        case 'WCF'
            Pts = [O' ; E'];
            color = zeros(2,3);
    end
    Pts = [Pts ; C'];
    color = [color ; [0 0 0]];
    
    % Cone
    switch CF
        case 'WCF'
            D = [Ellipse.xw ; Ellipse.yw ; Ellipse.zw]-repmat(Camera.Ew,1,length(Ellipse.xw));
            D = D./repmat(vecnorm(D),3,1);
            Delta = Camera.Ew-Ellipsoid.Cw;
            rts = [];
            for j=1:length(D)
                poly = [D(:,j)'*Ellipsoid.Aw*D(:,j) 2*D(:,j)'*Ellipsoid.Aw*Delta Delta'*Ellipsoid.Aw*Delta-1];
                rts = [rts mean(real(roots(poly)))];
            end
            P = repmat(Camera.Ew,1,length(Ellipse.xw))+rts.*D;
        case 'CCF'
            D = [Ellipse.xc ; Ellipse.yc ; Ellipse.zc]-repmat(Camera.Ec,1,length(Ellipse.xc));
            D = D./repmat(vecnorm(D),3,1);
            Delta = Camera.Ec-Info.Cc;
            rts = [];
            for j=1:length(D)
                poly = [D(:,j)'*Info.Ac*D(:,j) 2*D(:,j)'*Info.Ac*Delta Delta'*Info.Ac*Delta-1];
                rts = [rts mean(real(roots(poly)))];
            end
            P = repmat(Camera.Ec,1,length(Ellipse.xc))+rts.*D;
    end
    
    % ============== DISPLAY ===============
    % Points
    pcshow(Pts,color,'markerSize',30)
    
    % Image
    patch('Faces',[1 2 3 4],'Vertices',I','FaceColor',[0.5 0.5 0.5],'FaceAlpha',0.3);
    
    % Ellipse
    hold on
    line(x,y,z,'Color',ellipseColor,'LineWidth',2)
    
    % Ellipsoid
    hold on
    displayEllipsoid(Q,ellipsoidColor);
    
    % Coordinate frames
    if strcmp(CF,'WCF')
        hold on % World CF
        plot3(X_WCF(1,:),X_WCF(2,:),X_WCF(3,:),'LineWidth',2,'Color','Red');
        hold on
        plot3(Y_WCF(1,:),Y_WCF(2,:),Y_WCF(3,:),'LineWidth',2,'Color','Green');
        hold on
        plot3(Z_WCF(1,:),Z_WCF(2,:),Z_WCF(3,:),'LineWidth',2,'Color','Blue');
    end
    hold on % Camera CF
    plot3(X_CCF(1,:),X_CCF(2,:),X_CCF(3,:),'LineWidth',2,'Color','Red');
    hold on
    plot3(Y_CCF(1,:),Y_CCF(2,:),Y_CCF(3,:),'LineWidth',2,'Color','Green');
    hold on
    plot3(Z_CCF(1,:),Z_CCF(2,:),Z_CCF(3,:),'LineWidth',2,'Color','Blue');
    hold on % Ellipsoid CF
    plot3(X_ECF(1,:),X_ECF(2,:),X_ECF(3,:),'LineWidth',2,'Color','Red');
    hold on
    plot3(Y_ECF(1,:),Y_ECF(2,:),Y_ECF(3,:),'LineWidth',2,'Color','Green');
    hold on
    plot3(Z_ECF(1,:),Z_ECF(2,:),Z_ECF(3,:),'LineWidth',2,'Color','Blue');
    
    % Camera
    hold on
    if plotCam
        plotCamera('Size',camSize,'Orientation',CamOri,'Location',CamLoc,'Color',camColor);
    end

    % Cone
    for j=1:length(P)-1
        switch CF
            case 'WCF'
                v = [P(:,j:j+1)' ; Camera.Ew'];
            case 'CCF'
                v = [P(:,j:j+1)' ; Camera.Ec'];
        end
        f = [1 2 3];
        patch('Faces',f,'Vertices',v,'FaceColor',coneColor,'EdgeColor','None','FaceAlpha',0.3)
        hold on
    end
    switch CF
        case 'WCF'
            v = [P(:,end)' ; P(:,1)' ; Camera.Ew'];
        case 'CCF'
            v = [P(:,end)' ; P(:,1)' ; Camera.Ec'];
    end
    f = [1 2 3];
    patch('Faces',f,'Vertices',v,'FaceColor',coneColor,'EdgeColor','None','FaceAlpha',0.3)

    % Labels
    switch CF
        case 'WCF'
            xlabel('X_W');
            ylabel('Y_W');
            zlabel('Z_W');
        case 'CCF'
            xlabel('X_C');
            ylabel('Y_C');
            zlabel('Z_C');
    end
    if strcmp(CF,'WCF')
        text(O(1),O(2),O(3),'O','FontSize',20)
    end
    text(E(1),E(2),E(3),'E','FontSize',20)
    text(C(1),C(2),C(3),'C','FontSize',20)
    axis equal
    set(gcf,'color','white')
    set(gca,'color','white')
    set(gca,'XColor','black')
    set(gca,'YColor','black')
    set(gca,'ZColor','black')
end
