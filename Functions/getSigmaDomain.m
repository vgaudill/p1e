function [Domain] = getSigmaDomain(A,B_)
% A: ellipsoid orientation & shape
% B_: cone orientation & shape
% Domain: interval where sigma (scalar unknown in the Cone Alignment Equation) is defined

method = 'B_cone'; %'Aell' or 'B_cone'

switch method
    case 'Aell'
        lambdaA = eig(A,'vector');
        M = [1 1 1 ; lambdaA' ; lambdaA'.^2];
        invM = inv(M);

        trB_ = trace(B_);
        trB__1 = trace(inv(B_));
        detB_ = det(B_);
        trA = trace(A);
        trA_1 = trace(inv(A));
        detA = det(A);

        if(detB_<0) % sigma < 0
            mu0 = sqrt(-detB_/detA);
            for i=1:3
                D2(i,:) = [mu0*(invM(i,2)+invM(i,3)*trA) -invM(i,3)*trB_ -invM(i,1)*mu0*trB__1 invM(i,1)*trA_1+invM(i,2)];
                tmp = roots(D2(i,:));
                tmp(tmp<0) = NaN;
                rtsD2(i,:) = -tmp.^2;
                if(D2(i,1)<0) % D2i(sigma) increases/negative first (decreases when sigma --> -Inf)
                    def((i-1)*2+1:(i-1)*2+2,:) = [min(rtsD2(i,1),0) min(rtsD2(i,2),0) ; min(rtsD2(i,3),0) 0];
                else % D2i(sigma) decreases/positive first (increases when sigma --> -Inf)
                    def((i-1)*2+1:(i-1)*2+2,:) = [-Inf min(rtsD2(i,1),0) ; min(rtsD2(i,2),0) min(rtsD2(i,3),0)];
                end
            end
            if(sum(def(:,1)==-Inf)==3)
                error('Domain error for sigma.');
            else
                MINI = [];
                for j=1:6
                    tmp = def(j,1);
                    switch j
                        case {1,2}
                            c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                        case {3,4}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                        case {5,6}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                    end
                end
                MAXI = [];
                for j=1:6
                    tmp = def(j,2);
                    switch j
                        case {1,2}
                            c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                        case {3,4}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                        case {5,6}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                    end
                end
                if((length(MINI)>1)||(length(MAXI)>1))
                    error('Domain error for sigma (2).');
                else if((length(MINI)==0)||(length(MAXI)==0))
                        Domain = [];
                    else
                        Domain = linspace(MINI,MAXI,1000);
                    end
                end
            end
        else % sigma > 0
            mu0 = sqrt(detB_/detA);
            for i=1:3
                D2(i,:) = [mu0*(invM(i,2)+invM(i,3)*trA) invM(i,3)*trB_ invM(i,1)*mu0*trB__1 invM(i,1)*trA_1+invM(i,2)];
                tmp = roots(D2(i,:));
                tmp(tmp<0) = NaN;
                rtsD2(i,:) = tmp.^2;
                if(D2(i,1)<0) % D2i(sigma) decreases when sigma --> Inf
                    def((i-1)*2+1:(i-1)*2+2,:) = [0 max(rtsD2(i,1),0) max(rtsD2(i,2),0) ; max(rtsD2(i,3),0)];
                else % D2i(sigma) increases when sigma --> Inf
                    def((i-1)*2+1:(i-1)*2+2,:) = [max(rtsD2(i,1),0) max(rtsD2(i,2),0) ; max(rtsD2(i,3),0) Inf];
                end
            end
            if(sum(def(:,4)==Inf)==3)
                error('Domain error for sigma.');
            else
                MINI = [];
                for j=1:6
                    tmp = def(j,1);
                    switch j
                        case {1,2}
                            c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                        case {3,4}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                        case {5,6}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                    end
                end
                MAXI = [];
                for j=1:6
                    tmp = def(j,2);
                    switch j
                        case {1,2}
                            c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                        case {3,4}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                        case {5,6}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                    end
                end
                if((length(MINI)>1)||(length(MAXI)>1))
                    error('Domain error for sigma (2).');
                else if((length(MINI)==0)||(length(MAXI)==0))
                        Domain = [];
                    else
                        Domain = linspace(MINI,MAXI,1000);
                    end
                end
            end
        end

    case 'B_cone'

        clear D2 rtsD2 def

        lambdaB_ = eig(B_,'vector');
        M = [1 1 1 ; lambdaB_' ; lambdaB_'.^2];
        invM = inv(M);

        trB_ = trace(B_);
        trB__1 = trace(inv(B_));
        detB_ = det(B_);
        trA = trace(A);
        trA_1 = trace(inv(A));
        detA = det(A);

        if(detB_<0) % sigma < 0
            mu0 = sqrt(-detB_/detA);
            for i=1:3
                D2(i,:) = [-mu0*(invM(i,1)*trB__1+invM(i,2)) invM(i,1)*trA_1 mu0*invM(i,3)*trA -(invM(i,3)*trB_+invM(i,2))];

                tmp = real(roots(D2(i,:)));
    %             tmp(abs(imag(tmp))>0) = NaN;
                tmp(tmp<0) = NaN;
                rtsD2(i,:) = -tmp.^2;
                if(D2(i,1)<0) % D2i(sigma) increases/negative first (decreases when sigma --> -Inf)
                    def((i-1)*2+1:(i-1)*2+2,:) = [min(rtsD2(i,1),0) min(rtsD2(i,2),0) ; min(rtsD2(i,3),0) 0];
                else % D2i(sigma) decreases/positive first (increases when sigma --> -Inf)
                    def((i-1)*2+1:(i-1)*2+2,:) = [-Inf min(rtsD2(i,1),0) ; min(rtsD2(i,2),0) min(rtsD2(i,3),0)];
                end
            end
            if(sum(def(:,1)==-Inf)==3)
                error('Domain error for sigma.');
            else
                MINI = [];
                for j=1:6
                    tmp = def(j,1);
                    switch j
                        case {1,2}
                            c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                        case {3,4}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                        case {5,6}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                    end
                end
                MAXI = [];
                for j=1:6
                    tmp = def(j,2);
                    switch j
                        case {1,2}
                            c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                        case {3,4}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                        case {5,6}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                    end
                end
                if((length(MINI)>1)||(length(MAXI)>1))
                    error('Domain error for sigma (2).');
                else if((length(MINI)==0)||(length(MAXI)==0))
                        Domain = [];
                    else
                        Domain = linspace(MINI,MAXI,1000);
                    end
                end
            end
        else % sigma > 0
            mu0 = sqrt(detB_/detA);
            for i=1:3
                D2(i,:) = [-mu0*(invM(i,1)*trB__1+invM(i,2)) invM(i,1)*trA_1 mu0*invM(i,3)*trA invM(i,3)*trB_+invM(i,2)];
                tmp = roots(D2(i,:));
    %             tmp(abs(imag(tmp))>0) = NaN;
                tmp(tmp<0) = NaN;
                rtsD2(i,:) = tmp.^2;
                if(D2(i,1)<0) % D2i(sigma) decreases when sigma --> Inf
                    def((i-1)*2+1:(i-1)*2+2,:) = [0 max(rtsD2(i,1),0) max(rtsD2(i,2),0) ; max(rtsD2(i,3),0)];
                else % D2i(sigma) increases when sigma --> Inf
                    def((i-1)*2+1:(i-1)*2+2,:) = [max(rtsD2(i,1),0) max(rtsD2(i,2),0) ; max(rtsD2(i,3),0) Inf];
                end
            end
            if(sum(def(:,4)==Inf)==3)
                error('Domain error for sigma.');
            else
                MINI = [];
                for j=1:6
                    tmp = def(j,1);
                    switch j
                        case {1,2}
                            c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                        case {3,4}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                        case {5,6}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            if(c1&&c2)
                                MINI = [MINI tmp];
                            end
                    end
                end
                MAXI = [];
                for j=1:6
                    tmp = def(j,2);
                    switch j
                        case {1,2}
                            c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                        case {3,4}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                        case {5,6}
                            c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
                            c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
                            if(c1&&c2)
                                MAXI = [MAXI tmp];
                            end
                    end
                end
                if((length(MINI)>1)||(length(MAXI)>1))
                    error('Domain error for sigma (2).');
                else if((length(MINI)==0)||(length(MAXI)==0))
                        Domain = [];
                    else
                        Domain = linspace(MINI,MAXI,1000);
                    end
                end
            end
        end
end

% REMPLACE TOUT CE QUI PRECEDE, MAIS A CORRIGER CAR NE FONCTIONNE PAS.
% for ell=1:Nell
%     clear D2 rtsD2 def
% 
%     lambdaB_ = eig(B_{ell},'vector');
%     M = [1 1 1 ; lambdaB_' ; lambdaB_'.^2];
%     invM = inv(M);
% 
%     trB_ = trace(B_{ell});
%     trB__1 = trace(inv(B_{ell}));
%     detB_ = det(B_{ell});
%     trA = trace(Aw{ell});
%     trA_1 = trace(inv(Aw{ell}));
%     detA = det(Aw{ell});
%     
%     d = nthroot(detA/detB_,3);
%     for i=1:3
%         D2(i,:) = [-(invM(i,2)+invM(i,3)*trA) invM(i,3)*trB_*d -invM(i,1)*trB__1/d invM(i,1)*trA_1];
%         if i==2
%             D2(i,:) = (1/d)*D2(i,:);
%         else if i==3
%                 D2(i,:) = (1/d^2)*D2(i,:);
%             end
%         end
% 
%         tmp = roots(D2(i,:));
%         tmp(tmp>0) = NaN;%m<0
%         rtsD2(i,:) = sort(tmp,'ascend');
%         if(D2(i,1)>0) % D2i(m) increases/negative first (decreases when m --> -Inf)
%             def((i-1)*2+1:(i-1)*2+2,:) = [rtsD2(i,1) rtsD2(i,2) ; rtsD2(i,3) 0];
%         else % D2i(m) decreases/positive first (increases when m --> -Inf)
%             def((i-1)*2+1:(i-1)*2+2,:) = [-Inf rtsD2(i,1) ; rtsD2(i,2) rtsD2(i,3)];
%         end
%     end
%     if(sum(def(:,1)==-Inf)==3)
%         error('Domain error for sigma.');
%     else
%         def
%         MINI = [];
%         for j=1:6
%             tmp = def(j,1);
%             switch j
%                 case {1,2}
%                     c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
%                     c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
%                     if(c1&&c2)
%                         MINI = [MINI tmp];
%                     end
%                 case {3,4}
%                     c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
%                     c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
%                     if(c1&&c2)
%                         MINI = [MINI tmp];
%                     end
%                 case {5,6}
%                     c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
%                     c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
%                     if(c1&&c2)
%                         MINI = [MINI tmp];
%                     end
%             end
%         end
%         MAXI = [];
%         for j=1:6
%             tmp = def(j,2);
%             switch j
%                 case {1,2}
%                     c1 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
%                     c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
%                     if(c1&&c2)
%                         MAXI = [MAXI tmp];
%                     end
%                 case {3,4}
%                     c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
%                     c2 = ((def(5,1)<tmp)&&(tmp<def(5,2)))||((def(6,1)<tmp)&&(tmp<def(6,2)));
%                     if(c1&&c2)
%                         MAXI = [MAXI tmp];
%                     end
%                 case {5,6}
%                     c1 = ((def(1,1)<tmp)&&(tmp<def(1,2)))||((def(2,1)<tmp)&&(tmp<def(2,2)));
%                     c2 = ((def(3,1)<tmp)&&(tmp<def(3,2)))||((def(4,1)<tmp)&&(tmp<def(4,2)));
%                     if(c1&&c2)
%                         MAXI = [MAXI tmp];
%                     end
%             end
%         end
%         if((length(MINI)>1)||(length(MAXI)>1))
%             error('Domain error for sigma (2).');
%         else if((length(MINI)==0)||(length(MAXI)==0))
%                 Domain{ell} = [];
%             else
%                 if d<0
%                     MINI = d*MINI^2;
%                     MAXI = d*MAXI^2;
%                 else
%                     MINI = d*MAXI^2;
%                     MAXI = d*MINI^2;
%                 end
%                 Domain{ell} = linspace(MINI,MAXI,1000);
%             end
%         end
%     end

end

