function [] = displayP1ESolutions_triaxial_WCF(Ew,s_Ew,Ellipsoid,mode,ellipsoidColor,OPTIMIZE_MEMORY)

    for i=1:length(s_Ew)
        if(i>1)
            clf
        end

        Delta = Ew(:,i);
        C = Ellipsoid.Cw;
        E = Ew(:,i)';

        A = Ellipsoid.Aw;

        E_(i,:) = E;

        if strcmp(mode,'dynamic')
            if OPTIMIZE_MEMORY
                pcshow([E_;C'],[0 0 0],'markerSize',60)
            else
                pcshow(C',[0 0 0],'markerSize',60)
                if i<=(length(s_Ew)/2)
                    hold on
                    line(E_(:,1),E_(:,2),E_(:,3),'Color',[0 0 0],'LineWidth',3)
                else
                    hold on
                    line(E_(1:(length(s_Ew)/2),1),E_(1:(length(s_Ew)/2),2),E_(1:(length(s_Ew)/2),3),'Color',[0 0 0],'LineWidth',3)
                    hold on
                    line(E_((length(s_Ew)/2+1):end,1),E_((length(s_Ew)/2+1):end,2),E_((length(s_Ew)/2+1):end,3),'Color',[0 0 0],'LineWidth',3)
                end
            end
            hold on
            displayEllipsoid(Ellipsoid.Qw,ellipsoidColor);
            xlim([-8.51 4.51])
            ylim([-3.16 11.16])
            zlim([-6.31 8.4])
            text(E(1),E(2),E(3)+0.2,'E','FontSize',20)
            text(C(1),C(2),C(3),'C','FontSize',20)
%             text(-5.75,3.75,2.5,sprintf('a=%.02f',1/sqrt(Aell(1,1))),'FontSize',10,'Color',[0 0.5 1])
%             text(-5.75,3.75,2,sprintf('b=%.02f',1/sqrt(Aell(2,2))),'FontSize',10,'Color',[1 0 0])
%             text(-5.75,3.75,1.5,sprintf('c=%.02f',1/sqrt(Aell(3,3))),'FontSize',10,'Color',[0 1 0])
%             text(-3.5,6,11,'\bf Triaxial ellipsoid: locus of solutions','FontSize',15,'Color',[0 0 0])
            set(gcf,'color','white')
            set(gca,'color','white')
            set(gca,'XColor','black')
            set(gca,'YColor','black')
            set(gca,'ZColor','black')
            drawnow
        end
    end
    
    if strcmp(mode,'static')
        if OPTIMIZE_MEMORY
            pcshow([E_;C'],[0 0 0],'markerSize',60)
        else
            pcshow(C',[0 0 0],'markerSize',60)
            if i<=(length(s_Ew)/2)
                hold on
                line(E_(:,1),E_(:,2),E_(:,3),'Color',[0 0 0],'LineWidth',3)
            else
%                 hold on
%                 line(E_(1:(length(s_Ew)/2),1),E_(1:(length(s_Ew)/2),2),E_(1:(length(s_Ew)/2),3),'Color',[0 0 0],'LineWidth',3)
%                 hold on
%                 line(E_((length(s_Ew)/2+1):end,1),E_((length(s_Ew)/2+1):end,2),E_((length(s_Ew)/2+1):end,3),'Color',[0 0 0],'LineWidth',3)
                hold on
                line(E_(1:(length(s_Ew)/4),1),E_(1:(length(s_Ew)/4),2),E_(1:(length(s_Ew)/4),3),'Color',[0 0 0],'LineWidth',3)
                hold on
                line(E_((length(s_Ew)/4+1):(length(s_Ew)/2),1),E_((length(s_Ew)/4+1):(length(s_Ew)/2),2),E_((length(s_Ew)/4+1):(length(s_Ew)/2),3),'Color',[0 0 0],'LineWidth',3)
                hold on
                line(E_((length(s_Ew)/2+1):(3*length(s_Ew)/4),1),E_((length(s_Ew)/2+1):(3*length(s_Ew)/4),2),E_((length(s_Ew)/2+1):(3*length(s_Ew)/4),3),'Color',[0 0 0],'LineWidth',3)
                hold on
                line(E_((3*length(s_Ew)/4+1):end,1),E_((3*length(s_Ew)/4+1):end,2),E_((3*length(s_Ew)/4+1):end,3),'Color',[0 0 0],'LineWidth',3)
                
            end
        end
        hold on
        displayEllipsoid(Ellipsoid.Qw,ellipsoidColor);
        xlim([-8.51 4.51])
        ylim([-3.16 11.16])
        zlim([-6.31 8.4])
        text(C(1),C(2),C(3),'C','FontSize',20)
        Aell = sort(eig(Ellipsoid.Aw));
        text(-8.25,11,-3,sprintf('a=%.02f',1/sqrt(Aell(1))),'FontSize',10,'Color',[0 0.5 1])
        text(-8.25,11,-4,sprintf('b=%.02f',1/sqrt(Aell(2))),'FontSize',10,'Color',[1 0 0])
        text(-8.25,11,-5,sprintf('c=%.02f',1/sqrt(Aell(3))),'FontSize',10,'Color',[0 1 0])
        text(-6.25,11,10,'\bf Triaxial ellipsoid: locus of solutions','FontSize',15,'Color',[0 0 0])
        set(gcf,'color','white')
        set(gca,'color','white')
        set(gca,'XColor','black')
        set(gca,'YColor','black')
        set(gca,'ZColor','black')
        drawnow
    end
end