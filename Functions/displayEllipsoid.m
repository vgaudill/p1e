function [] = displayEllipsoid(Q,color)

    % Normalize the ellipsoid
    Q = Q/(-Q(4,4));
    
    % Compute ellipsoid's center
    C = squeeze(-Q(1:3,4));
    
    % Center the ellipsoid
    T = [1 0 0 -C(1); 0 1 0 -C(2); 0 0 1 -C(3); 0 0 0 1];
    Qcent = T*Q*T';

    % Compute ellipsoid's semi-axis lengths (=radii?)
    [V,D] = eig(Qcent(1:3,1:3));
    [~,order] = sort(diag(D));
    V = V(:,order);
    if det(V)<0
        V = -V;
    end
    if V(1,1)<0
        V(:,1:2) = -V(:,1:2);
    end
    if V(3,2)<0
        V(:,2:3) = -V(:,2:3);
    end
    D = D(order,order);
    a = sqrt(abs(D(1,1)));
    b = sqrt(abs(D(2,2)));
    c = sqrt(abs(D(3,3)));

    if not(isreal(a) && isreal(b) && isreal(c))
        disp('Problem!');
    else
        % Compute points on ellipsoid's surface
        N = 20;
        [X,Y,Z] = ellipsoid(0,0,0,a,b,c,N);
        %  rotate and center the ellipsoid to the actual center point
        %------------------------------------------------------------
        XX = zeros(N+1,N+1);
        YY = zeros(N+1,N+1);
        ZZ = zeros(N+1,N+1);
        for k = 1:length(X)
            for j = 1:length(X)
                point = [X(k,j) Y(k,j) Z(k,j)]';
                P = V * point; % rotation
                XX(k,j) = P(1)+C(1); % translation
                YY(k,j) = P(2)+C(2);
                ZZ(k,j) = P(3)+C(3);
            end
        end
        
        % Plot the ellipsoid
        % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        surface(XX,YY,ZZ,'FaceColor', color, 'EdgeColor', color, 'linewidth', 1, 'EdgeAlpha',0.3);

        %axis equal;
        alpha(0.2);
%         hold on;
    end

end