% This source code is to reproduce the video named 'P1E.mp4'
% (https://members.loria.fr/MOBerger/Documents/P1E.mp4).

clearvars; close all; clc;

PART = 'CCF-transition'; 
% 'CCF-triaxial': triaxial ellipsoid solutions in Camera Coordinate Frame
% 'CCF-transition': ellipsoid solutions when transitioning towards sphere
% 'WCF-transition': camera solutions when transitioning towards sphere

addpath(genpath('Functions'))

switch PART
    case 'CCF-triaxial'
        Ntri = 18;
        Nsph = 12;
        abc_values = [repmat([4 2 1],Ntri,1);repmat([4 2 2],Nsph,1)];
        abc_values(1:Ntri,3) = [1 1.1 1.2 1.3 1.4 1.5 1.6 1.65 1.7 1.74 1.77 1.8 1.82 1.85 1.88 1.91 1.95 2]';
        abc_values(Ntri+1:Ntri+Nsph,1) = [4 4:-0.2:2]';
        abc_values(Ntri+Nsph,1) = 2.001;
        abc_values(Ntri+Nsph+1,:) = [2 2 2];
        abc_values = abc_values';
        
        figure
        % Define Ellipsoid
        a = abc_values(1,1); b = abc_values(2,1); c = abc_values(3,1);
        Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
        Cw = [-2 4 1]';
        Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

        % Define Camera
        Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
        Ew = [-1 -1 -4]';
        f = 1;
        Cam = createCamera(Ew,Rw_c,f);

        % Project Ellipsoid
        Ellipse = projectEllipsoid(Ellipsoid,Cam);

        % Solve P1E
        B_ = computeBackprojCone(Ellipse);
        s = getSigmaDomain(Ellipsoid.Aw,B_);
        [Cc,s_Cc] = computeEllipsoidPositionSolutions(Ellipsoid,Cam,B_,s);

        % Display solutions in the Camera Coordinate Frame (CCF)
        mode = 'dynamic';
        sizeAxes = 0.5;
        widthImage = 2;
        heightImage = 2;
        ellipsoidColor = [0.8500 0.3250 0.0980];
        ellipseColor = [0.8500 0.3250 0.0980];
        coneColor = [0.8500 0.3250 0.0980];
        DISPLAY_CONTACT = false; % considered only in 'dynamic' mode
        OPTIMIZE_MEMORY = true;
        F1_CCF = displayP1ESolutions_triaxial_CCF_video(Cc,s_Cc,Ellipse,B_,Cam,mode,sizeAxes,widthImage,heightImage,ellipsoidColor,ellipseColor,coneColor,DISPLAY_CONTACT,OPTIMIZE_MEMORY);
        
        % F1_CCF = F1_CCF(1:3:end);
        % save('F1_CCF.mat','F1_CCF');
        % writerObj = VideoWriter('Transition_CCF');
        % writerObj.Quality = 99;
        % writerObj.FrameRate = 3;
        % open(writerObj);
        % writeVideo(writerObj, F1_CCF)
        % close(writerObj);
    case 'CCF-transition'
        Ntri = 18;
        Nsph = 12;
        abc_values = [repmat([4 2 1],Ntri,1);repmat([4 2 2],Nsph,1)];
        abc_values(1:Ntri,3) = [1 1.1 1.2 1.3 1.4 1.5 1.6 1.65 1.7 1.74 1.77 1.8 1.82 1.85 1.88 1.91 1.95 2]';
        abc_values(Ntri+1:Ntri+Nsph,1) = [4 4:-0.2:2]';
        abc_values(Ntri+Nsph,1) = 2.001;
        abc_values(Ntri+Nsph+1,:) = [2 2 2];
        abc_values = abc_values';
        
        figure
        count = 0;
        for abc=abc_values(:,1:end)
            count = count+1;
            clf()

            % Define Ellipsoid
            a = abc(1); b = abc(2); c = abc(3);
            Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
            Cw = [-2 4 1]';
            Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

            % Define Camera
            Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
            Ew = [-1 -1 -4]';
            f = 1;
            Cam = createCamera(Ew,Rw_c,f);

            % Project Ellipsoid
            Ellipse = projectEllipsoid(Ellipsoid,Cam);

            % Solve P1E
            B_ = computeBackprojCone(Ellipse);
            if count<=Ntri
                s = getSigmaDomain(Ellipsoid.Aw,B_);
                [Cc,s_Cc] = computeEllipsoidPositionSolutions(Ellipsoid,Cam,B_,s);

                % Display solutions in the Camera Coordinate Frame (CCF)
                mode = 'static';
                sizeAxes = 0.5;
                widthImage = 2;
                heightImage = 2;
                ellipsoidColor = [0.8500 0.3250 0.0980];
                ellipseColor = [0.8500 0.3250 0.0980];
                coneColor = [0.8500 0.3250 0.0980];
                DISPLAY_CONTACT = false; % considered only in 'dynamic' mode
                OPTIMIZE_MEMORY = true;
                displayP1ESolutions_triaxial_CCF_video(Cc,s_Cc,Ellipse,B_,Cam,mode,sizeAxes,widthImage,heightImage,ellipsoidColor,ellipseColor,coneColor,DISPLAY_CONTACT,OPTIMIZE_MEMORY);
                drawnow()
            else
                if count<=(Ntri+Nsph)
                    s = getSigma_spheroid(Ellipsoid.Aw,B_); % only one scalar for spheroid, not an interval
                    [Cc,s_Cc] = computeEllipsoidPositionSolutions(Ellipsoid,Cam,B_,repmat(s,1,1000)+10^(-7)*rand(1,1000)-5*10^(-8));

                    % Display solutions in the Camera Coordinate Frame (CCF)
                    widthImage = 2;
                    heightImage = 2;
                    ellipsoidColor = [0.8500 0.3250 0.0980];
                    ellipseColor = [0.8500 0.3250 0.0980];
                    DISPLAY_SPHEROIDS = true;
                    DISPLAY_CONTACT = false; % considered only if DISPLAY_SPHEROIDS
                    if count==(Ntri+1)
                        DISPLAY_RADII_ENDPOINTS = true;
                    else
                        DISPLAY_RADII_ENDPOINTS = false;
                    end
                    displayP1ESolutions_spheroid_CCF_video(Cc,s_Cc,Ellipse,B_,Cam,widthImage,heightImage,ellipsoidColor,ellipseColor,DISPLAY_SPHEROIDS,DISPLAY_CONTACT,DISPLAY_RADII_ENDPOINTS);
                    drawnow()
                else
                    sizeAxes = 0.5;
                    widthImage = 2;
                    heightImage = 2;
                    plotCam = false;
                    camColor = 'black';
                    camSize = 0.2;
                    coneColor = 'red';
                    displayP1EScene_video('CCF',Ellipsoid,Ellipse,Cam,ellipsoidColor,ellipseColor,coneColor,sizeAxes,widthImage,heightImage,plotCam,camColor,camSize);
                    drawnow()
                end
            end
            F(count) = getframe();
        end
        
        % F2_CCF = F;
        % F2_CCF(end+1) = F(end);
        % save('F2_CCF.mat','F2_CCF');
        % writerObj = VideoWriter('Transition_CCF2');
        % writerObj.Quality = 99;
        % writerObj.FrameRate = 0.5;
        % open(writerObj);
        % writeVideo(writerObj, F2_CCF)
        % close(writerObj);
    case 'WCF-transition'
        Ntri = 18;
        Nsph = 12;
        abc_values = [repmat([4 2 1],Ntri,1);repmat([4 2 2],Nsph,1)];
        abc_values(1:Ntri,3) = [1 1.1 1.2 1.3 1.4 1.5 1.6 1.65 1.7 1.74 1.77 1.8 1.82 1.85 1.88 1.91 1.95 1.99]';
        abc_values(Ntri+1:Ntri+Nsph,1) = [4 4:-0.2:2]';
        abc_values(Ntri+Nsph,1) = 2.01;
        abc_values(Ntri+Nsph+1,:) = [2 2 2];
        abc_values = abc_values';

        figure
        count = 0;
        for abc=abc_values
            count = count+1;
            clf()

            % Define Ellipsoid
            a = abc(1); b = abc(2); c = abc(3);
            Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
            Cw = [-2 4 1]';
            Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

            % Define Camera
            Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
            Ew = [-1 -1 -4]';
            f = 1;
            Cam = createCamera(Ew,Rw_c,f);

            % Project Ellipsoid
            Ellipse = projectEllipsoid(Ellipsoid,Cam);

            % Solve P1E
            B_ = computeBackprojCone(Ellipse);
            if count<=Ntri
                s = getSigmaDomain(Ellipsoid.Aw,B_);
                [Ew,s_Ew] = computeCameraPositionSolutions(Ellipsoid,Cam,B_,s);

                % Display solutions in the World Coordinate Frame (WCF)
                mode = 'static';
                ellipsoidColor = [0.8500 0.3250 0.0980];
                OPTIMIZE_MEMORY = true;
                displayP1ESolutions_triaxial_WCF_video(Ew,s_Ew,Ellipsoid,mode,ellipsoidColor,OPTIMIZE_MEMORY);
                drawnow()
            else
                if count<=(Ntri+Nsph)
                    s = getSigma_spheroid(Ellipsoid.Aw,B_); % only one scalar for spheroid, not an interval
                    [Cc,s_Cc] = computeEllipsoidPositionSolutions(Ellipsoid,Cam,B_,repmat(s,1,1000));%+10^(-7)*rand(1,1000)-5*10^(-8));

                    % Display solutions in the World Coordinate Frame (WCF)
                    ellipsoidColor = [0.8500 0.3250 0.0980];
                    displayP1ESolutions_spheroid_WCF_video(Cc,s_Cc,Ellipsoid,B_,ellipsoidColor);
                    drawnow()
                else
                    ellipsoidColor = [0.8500 0.3250 0.0980];
                    displayP1ESolutions_sphere_WCF(Ellipsoid,B_,ellipsoidColor)
                    drawnow()
                end
            end
            F(count) = getframe();
        end
        
        % F_WCF = F;
        % F_WCF(end+1) = F(end);
        % save('F_WCF.mat','F_WCF');
        % writerObj = VideoWriter('Transition_WCF');
        % writerObj.Quality = 99;
        % writerObj.FrameRate = 0.5;
        % open(writerObj);
        % writeVideo(writerObj, F_WCF)
        % close(writerObj);
end