# Perspective-1-Ellipsoid

This repository serves as an attachment to the IJCV 2023 paper entitled [Perspective-1-Ellipsoid: Formulation, Analysis and Solutions of the Camera Pose Estimation Problem from One Ellipse-Ellipsoid Correspondence](https://vincentgaudilliere.github.io/publication/perspective-1-ellipsoid-formulation-analysis-and-solutions-of-the-camera-pose-estimation-problem-from-one-ellipse-ellipsoid-correspondence/), from Vincent Gaudillière, Gilles Simon and Marie-Odile Berger.

`Tested on Matlab R2020b`
