% This source code serves as an attachment to the paper entitled
% "Perspective-1-Ellipsoid: Formulation, Analysis and Solutions of the
% Camera Pose Estimation Problem from One Ellipse-Ellipsoid Correspondence",
% from Vincent Gaudillière, Gilles Simon and Marie-Odile Berger.
% - Tested on Matlab R2020b.

clearvars; close all; clc;

addpath(genpath('Functions'))

% Set the experience index to reproduce from the paper:
Xp = 6;
% Xp=1 -> Display a tri-axial ellipsoid along with its projected ellipse
% Xp=2 -> Display a spheroid with its projected ellipse
% Xp=3 -> Display a sphere with its projected ellipse
% Xp=4 -> Display P1E solutions for a tri-axial ellipsoid (camera coordinate frame)
% Xp=5 -> Display P1E solutions for a tri-axial ellipsoid (world coordinate frame)
% Xp=6 -> Display P1E solutions for a spheroid (camera coordinate frame)
% Xp=7 -> Display P1E solutions for a spheroid (world coordinate frame)

switch Xp
    case 1
        % Define Ellipsoid
        a = 4; b = 2; c = 1;
        Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
        Cw = [-2 4 1]';
        Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

        % Define Camera
        Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
        Ew = [-1 -1 -4]';
        f = 1;
        Cam = createCamera(Ew,Rw_c,f);

        % Project Ellipsoid
        Ellipse = projectEllipsoid(Ellipsoid,Cam);

        % Display scene
        CF = 'CCF'; % Coordinate frame: World ('WCF') or Camera ('CCF')
        sizeAxes = 0.5;
        widthImage = 2;
        heightImage = 2;
        plotCam = false;
        camColor = 'black';
        camSize = 0.2;
        ellipsoidColor = [0.5 0 0];
        ellipseColor = [0.6 0 0];
        coneColor = 'red';
        figure
        displayP1EScene(CF,Ellipsoid,Ellipse,Cam,ellipsoidColor,ellipseColor,coneColor,sizeAxes,widthImage,heightImage,plotCam,camColor,camSize);
        
    case 2
        % Define Ellipsoid
        a = 4; b = 2; c = 2;
        Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
        Cw = [-2 4 1]';
        Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

        % Define Camera
        Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
        Ew = [-1 -1 -4]';
        f = 1;
        Cam = createCamera(Ew,Rw_c,f);

        % Project Ellipsoid
        Ellipse = projectEllipsoid(Ellipsoid,Cam);

        % Display scene
        CF = 'CCF'; % Coordinate frame: World ('WCF') or Camera ('CCF')
        sizeAxes = 0.5;
        widthImage = 2;
        heightImage = 2;
        plotCam = false;
        camColor = 'black';
        camSize = 0.2;
        ellipsoidColor = [0.5 0 0];
        ellipseColor = [0.6 0 0];
        coneColor = 'red';
        figure
        displayP1EScene(CF,Ellipsoid,Ellipse,Cam,ellipsoidColor,ellipseColor,coneColor,sizeAxes,widthImage,heightImage,plotCam,camColor,camSize);

    case 3
        % Define Ellipsoid
        a = 2; b = 2; c = 2;
        Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
        Cw = [-2 4 1]';
        Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

        % Define Camera
        Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
        Ew = [-1 -1 -4]';
        f = 1;
        Cam = createCamera(Ew,Rw_c,f);

        % Project Ellipsoid
        Ellipse = projectEllipsoid(Ellipsoid,Cam);

        % Display scene
        CF = 'CCF'; % Coordinate frame: World ('WCF') or Camera ('CCF')
        sizeAxes = 0.5;
        widthImage = 2;
        heightImage = 2;
        plotCam = false;
        camColor = 'black';
        camSize = 0.2;
        ellipsoidColor = [0.5 0 0];
        ellipseColor = [0.6 0 0];
        coneColor = 'red';
        figure
        displayP1EScene(CF,Ellipsoid,Ellipse,Cam,ellipsoidColor,ellipseColor,coneColor,sizeAxes,widthImage,heightImage,plotCam,camColor,camSize);

    case 4
        % Define Ellipsoid
        a = 4; b = 2; c = 1;
        Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
        Cw = [-2 4 1]';
        Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

        % Define Camera
        Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
        Ew = [-1 -1 -4]';
        f = 1;
        Cam = createCamera(Ew,Rw_c,f);

        % Project Ellipsoid
        Ellipse = projectEllipsoid(Ellipsoid,Cam);

        % Solve P1E
        B_ = computeBackprojCone(Ellipse);
        s = getSigmaDomain(Ellipsoid.Aw,B_);
        [Cc,s_Cc] = computeEllipsoidPositionSolutions(Ellipsoid,Cam,B_,s);
        
        % Display solutions in the Camera Coordinate Frame (CCF)
        mode = 'static';
        sizeAxes = 0.5;
        widthImage = 2;
        heightImage = 2;
        ellipsoidColor = [0.8500 0.3250 0.0980];
        ellipseColor = [0.8500 0.3250 0.0980];
        coneColor = [0.8500 0.3250 0.0980];
        DISPLAY_CONTACT = false; % considered only in 'dynamic' mode
        OPTIMIZE_MEMORY = true;
        figure
        displayP1ESolutions_triaxial_CCF(Cc,s_Cc,Ellipse,B_,Cam,mode,sizeAxes,widthImage,heightImage,ellipsoidColor,ellipseColor,coneColor,DISPLAY_CONTACT,OPTIMIZE_MEMORY);
    
    case 5
        % Define Ellipsoid
        a = 4; b = 2; c = 1;
        Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
        Cw = [-2 4 1]';
        Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

        % Define Camera
        Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
        Ew = [-1 -1 -4]';
        f = 1;
        Cam = createCamera(Ew,Rw_c,f);

        % Project Ellipsoid
        Ellipse = projectEllipsoid(Ellipsoid,Cam);

        % Solve P1E
        B_ = computeBackprojCone(Ellipse);
        s = getSigmaDomain(Ellipsoid.Aw,B_);
        [Ew,s_Ew] = computeCameraPositionSolutions(Ellipsoid,Cam,B_,s);
        
        % Display solutions in the World Coordinate Frame (WCF)
        mode = 'static';
        ellipsoidColor = [0.8500 0.3250 0.0980];
        OPTIMIZE_MEMORY = false;
        figure
        displayP1ESolutions_triaxial_WCF(Ew,s_Ew,Ellipsoid,mode,ellipsoidColor,OPTIMIZE_MEMORY);
    
    case 6
        % Define Ellipsoid
        a = 4; b = 2; c = 2;
        Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
        Cw = [-2 4 1]';
        Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

        % Define Camera
        Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
        Ew = [-1 -1 -4]';
        f = 1;
        Cam = createCamera(Ew,Rw_c,f);

        % Project Ellipsoid
        Ellipse = projectEllipsoid(Ellipsoid,Cam);

        % Solve P1E
        B_ = computeBackprojCone(Ellipse);
        s = getSigma_spheroid(Ellipsoid.Aw,B_); % only one scalar for spheroid, not an interval
        [Cc,s_Cc] = computeEllipsoidPositionSolutions(Ellipsoid,Cam,B_,repmat(s,1,1000)+10^(-7)*rand(1,1000)-5*10^(-8));
        
        % Display solutions in the Camera Coordinate Frame (CCF)
        widthImage = 2;
        heightImage = 2;
        ellipsoidColor = [0.8500 0.3250 0.0980];
        ellipseColor = [0.8500 0.3250 0.0980];
        DISPLAY_SPHEROIDS = true;
        DISPLAY_CONTACT = true; % considered only if DISPLAY_SPHEROIDS
        DISPLAY_RADII_ENDPOINTS = true;
        figure
        displayP1ESolutions_spheroid_CCF(Cc,s_Cc,Ellipse,B_,Cam,widthImage,heightImage,ellipsoidColor,ellipseColor,DISPLAY_SPHEROIDS,DISPLAY_CONTACT,DISPLAY_RADII_ENDPOINTS);
    
    case 7
        % Define Ellipsoid
        a = 4; b = 2; c = 2;
        Rw_ell = SpinCalc('EA131toDCM',[15 35 5],1,0)';
        Cw = [-2 4 1]';
        Ellipsoid = createEllipsoid(Cw,Rw_ell,a,b,c);

        % Define Camera
        Rw_c = SpinCalc('EA131toDCM',[-45 10 10],1,0)';
        Ew = [-1 -1 -4]';
        f = 1;
        Cam = createCamera(Ew,Rw_c,f);

        % Project Ellipsoid
        Ellipse = projectEllipsoid(Ellipsoid,Cam);

        % Solve P1E
        B_ = computeBackprojCone(Ellipse);
        s = getSigma_spheroid(Ellipsoid.Aw,B_); % only one scalar for spheroid, not an interval
        [Cc,s_Cc] = computeEllipsoidPositionSolutions(Ellipsoid,Cam,B_,repmat(s,1,1000)+10^(-7)*rand(1,1000)-5*10^(-8));
        
        % Display solutions in the World Coordinate Frame (WCF)
        ellipsoidColor = [0.8500 0.3250 0.0980];
        figure
        displayP1ESolutions_spheroid_WCF(Cc,s_Cc,Ellipsoid,B_,ellipsoidColor);
end